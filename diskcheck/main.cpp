#include   <windows.h>
#include   <stdio.h>   
#include   <conio.h> 
#include   <winioctl.h>
#include   <setupapi.h>

#include "ff.h"

#define VERSION_VER "1.0.2"
#define FATFS_VER "R0.14a"
#define MAX_DEVICE  10
#define INTERFACE_DETAIL_SIZE    (1024)
DEFINE_GUID(GUID_DEVINTERFACE_DISK, 0x53f56307L, 0xb6bf, 0x11d0, 0x94, 0xf2, 0x00, 0xa0, 0xc9, 0x1e, 0xfb, 0x8b);

#pragma comment (lib, "Setupapi.lib")

char fs_type_str[][16] = {
	{"unknow"},
	{"FS_FAT12"},
	{"FS_FAT16"},
	{"FS_FAT32"},
	{"FS_EXFAT"},
};

PSP_DEVICE_INTERFACE_DETAIL_DATA psp_device_detail = NULL;
int detail_buff[1024] = { 0 };

extern "C"
{
DWORD BytesPerSector = 512;
char strDriverIndx[128] = { 0 };

char *GetDriverIndx(void)
{
	return strDriverIndx;
}

}
char current_vol[128] = { 0 };
FATFS gFatfs_sd;   /* File system object */
DIR root_dir;
FILINFO Finfo;
FRESULT res;

int device_cnt = 0;

/******************************************************************************
* Function: get device path from GUID
* input: lpGuid, GUID pointer
* output: pszDevicePath, device paths
* return: Succeed, the amount of found device paths
*         Fail, -1
******************************************************************************/
DWORD GetDevicePath(LPGUID lpGuid, WCHAR **pszDevicePath)
{
	HDEVINFO hDevInfoSet;
	SP_DEVICE_INTERFACE_DATA ifdata;
	DWORD nCount;
	BOOL result;

	//get a handle to a device information set
	hDevInfoSet = SetupDiGetClassDevs(
		lpGuid,      // class GUID
		NULL,        // Enumerator
		NULL,        // hwndParent
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE  | DIGCF_ALLCLASSES  // present devices
	);

	//fail...
	if (hDevInfoSet == INVALID_HANDLE_VALUE)
	{
		fprintf(stderr, "IOCTL_STORAGE_GET_DEVICE_NUMBER Error: %ld\n", GetLastError());
		return (DWORD)-1;
	}

	psp_device_detail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)detail_buff;
	psp_device_detail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

	nCount = 0;
	result = TRUE;

	// device index = 0, 1, 2... test the device interface one by one
	while (result)
	{
		ifdata.cbSize = sizeof(ifdata);

		//enumerates the device interfaces that are contained in a device information set
		result = SetupDiEnumDeviceInterfaces(
			hDevInfoSet,     // DeviceInfoSet
			NULL,            // DeviceInfoData
			lpGuid,          // GUID
			nCount,   // MemberIndex
			&ifdata        // DeviceInterfaceData
		);
		if (result)
		{
			// get details about a device interface
			result = SetupDiGetDeviceInterfaceDetail(
				hDevInfoSet,    // DeviceInfoSet
				&ifdata,        // DeviceInterfaceData
				psp_device_detail,        // DeviceInterfaceDetailData
				INTERFACE_DETAIL_SIZE,    // DeviceInterfaceDetailDataSize
				NULL,           // RequiredSize
				NULL          // DeviceInfoData
			);
			if (result)
			{
				WCHAR *cc;
				// copy the path to output buffer
				wcscpy(pszDevicePath[nCount], psp_device_detail->DevicePath);
				cc = wcschr(psp_device_detail->DevicePath, '{');
				if (cc)
				{
					*cc = '\0';
					*(cc+1) = '\0';
				}

				printf("%d-->%ws\n", nCount, psp_device_detail->DevicePath);
				nCount++;
			}
		}
	}

	device_cnt = nCount;
	(void)SetupDiDestroyDeviceInfoList(hDevInfoSet);

	return nCount;
}

/******************************************************************************
* Function: get all present disks' physical number
* input: N/A
* output: ppDisks, array of disks' physical number
* return: Succeed, the amount of present disks
*         Fail, -1
******************************************************************************/
DWORD GetAllPresentDisks(DWORD **ppDisks)
{
	WCHAR *szDevicePath[MAX_DEVICE];        // device path
	DWORD nDevice;
	HANDLE hDevice;
	STORAGE_DEVICE_NUMBER number;
	BOOL result;
	DWORD readed;
	WORD i, j;

	for (i = 0; i < MAX_DEVICE; i++)
	{
		szDevicePath[i] = (WCHAR *)malloc(INTERFACE_DETAIL_SIZE);
		if (NULL == szDevicePath[i])
		{
			for (j = 0; j < i; j++)
			{
				free(szDevicePath[i]);
			}
			return (DWORD)-1;
		}
	}

	// get the device paths
	nDevice = GetDevicePath(const_cast<LPGUID>(&GUID_DEVINTERFACE_DISK), szDevicePath);
	if ((DWORD)-1 == nDevice)
	{
		for (i = 0; i < MAX_DEVICE; i++)
		{
			free(szDevicePath[i]);
		}
		return (DWORD)-1;
	}

	*ppDisks = (DWORD *)malloc(sizeof(DWORD) * nDevice);

	// get the disk's physical number one by one
	for (i = 0; i < nDevice; i++)
	{
		hDevice = CreateFile(
			szDevicePath[i], // drive to open
			GENERIC_READ | GENERIC_WRITE,     // access to the drive
			FILE_SHARE_READ | FILE_SHARE_WRITE, //share mode
			NULL,             // default security attributes
			OPEN_EXISTING,    // disposition
			0,                // file attributes
			NULL            // do not copy file attribute
		);
		if (hDevice == INVALID_HANDLE_VALUE) // cannot open the drive
		{
			for (j = 0; j < MAX_DEVICE; j++)
			{
				free(szDevicePath[j]);
			}
			free(*ppDisks);
			fprintf(stderr, "CreateFile() Error: %ld\n", GetLastError());
			return DWORD(-1);
		}
		result = DeviceIoControl(
			hDevice,                // handle to device
			IOCTL_STORAGE_GET_DEVICE_NUMBER, // dwIoControlCode
			NULL,                            // lpInBuffer
			0,                               // nInBufferSize
			&number,           // output buffer
			sizeof(number),         // size of output buffer
			&readed,       // number of bytes returned
			NULL      // OVERLAPPED structure
		);
		if (!result) // fail
		{
			fprintf(stderr, "IOCTL_STORAGE_GET_DEVICE_NUMBER Error: %ld\n", GetLastError());
			for (j = 0; j < MAX_DEVICE; j++)
			{
				free(szDevicePath[j]);
			}
			free(*ppDisks);
			(void)CloseHandle(hDevice);
			return (DWORD)-1;
		}
		*(*ppDisks + i) = number.DeviceNumber;
		// printf("%d\n",number.DeviceNumber);
		(void)CloseHandle(hDevice);
	}

	for (i = 0; i < MAX_DEVICE; i++)
	{
		free(szDevicePath[i]);
	}
	return nDevice;
}

int main(int argc, CHAR* argv[])
{
	DWORD *pDisk;
	
	char dnum[4];
	int t;
	printf("diskcheck by fatfs, version: %s\n", VERSION_VER);
	printf("fatfs version: %s\n", FATFS_VER);
	printf("view at vscode\n");
	t = GetAllPresentDisks(&pDisk);
	printf("Please select the physical_driver num(0~%d): \n", device_cnt-1);
	dnum[0] = _getch();
	if (atoi(dnum) >= device_cnt)
	{
		printf("physical_driver num invalid!\n");
		return -1;
	}

	sprintf_s(strDriverIndx, 127, "\\\\.\\PhysicalDrive%d", pDisk[atoi(dnum)]);
	printf("%s\n", strDriverIndx);
	HANDLE hPhysicalDriveIOCTL = CreateFileA(strDriverIndx, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	if (hPhysicalDriveIOCTL != INVALID_HANDLE_VALUE) {
		DWORD lpOutBufferMaxLen = 1024 * 1024 * 3;
		char* lpOutBuffer = new char[lpOutBufferMaxLen];
		DWORD BytesReturned = 0;
		memset(lpOutBuffer, 0, lpOutBufferMaxLen);
		BOOL cs = DeviceIoControl(hPhysicalDriveIOCTL, IOCTL_DISK_GET_DRIVE_GEOMETRY_EX, 0, 0, lpOutBuffer, lpOutBufferMaxLen, &BytesReturned, 0);
		if (cs && (BytesReturned > 0)) {
			// 由于使用了 IOCTL_DISK_GET_DRIVE_GEOMETRY_EX 指令，所以输出缓冲区里的数据格式为DISK_GEOMETRY_EX
			DISK_GEOMETRY_EX* diskgeometry = (DISK_GEOMETRY_EX*)lpOutBuffer;
			// 磁盘大小
			unsigned __int64 DiskSize = diskgeometry->Geometry.Cylinders.QuadPart * diskgeometry->Geometry.TracksPerCylinder
				* diskgeometry->Geometry.SectorsPerTrack * diskgeometry->Geometry.BytesPerSector;

			// 磁盘类型 diskgeometry->Geometry.MediaType，值：FixedMedia是磁盘；RemovableMedia是移动磁盘，或U盘光盘之类的
			//可参考MSDN：MEDIA_TYPE
				// 柱面数 diskgeometry->Geometry.Cylinders
				// 每个柱面的轨道数 diskgeometry->Geometry.TracksPerCylinder
				// 每个磁道的扇区数 diskgeometry->Geometry.SectorsPerTrack
				// 每个扇区的字节数 diskgeometry->Geometry.BytesPerSector
			if (diskgeometry->Geometry.MediaType == 11)
				printf("Removable media other than floppy.\n");
			else if (diskgeometry->Geometry.MediaType == 12)
				printf("Fixed hard disk media.\n");
			else
				printf("Other Disk.");
			printf("sector size: %d, total size: %.2fGB\n", diskgeometry->Geometry.BytesPerSector, (float)DiskSize/1024/1024/1024);
			BytesPerSector = diskgeometry->Geometry.BytesPerSector;
		}
		else
		{
			printf("BytesReturned ret error?\n");
			delete[] lpOutBuffer;
			CloseHandle(hPhysicalDriveIOCTL);
			return -1;
		}

		delete[] lpOutBuffer;
		CloseHandle(hPhysicalDriveIOCTL);
	}
	else
	{
		printf("error: INVALID_HANDLE_VALUE\n");
		return -1;
	}
	
	//wcscpy(current_vol, (const wchar_t*)"0:/");
	strcpy_s((char*)current_vol, 64, (const char*)"0:/");
	if (f_mount(&gFatfs_sd, (const TCHAR *)current_vol, 1) == 0)
	{
		printf("device mount to 0:/--> OK\n");
		printf("fs_type is %s\n", fs_type_str[gFatfs_sd.fs_type]);
	}
	else
	{
		printf("device mount to 0:/--> Failed\n");
	}

	{
		if ((res = f_opendir(&root_dir, (const TCHAR *)"/")) != FR_OK)
		{
			printf("cannot open root dir. %d\n", res);
			//printf("%c, %c, %c\n",  current_vol[0], current_vol[1], current_vol[2]);
			return -1;
		}

		while (((res = f_readdir(&root_dir, &Finfo)) == FR_OK) && Finfo.fname[0])
		{
			wprintf(L"-->%s\n", Finfo.fname);
		}
	}

	printf("finished\n");
	_getch();
	return 0;
}